<?php

class Application {
    //put your code here
    private $connect;
   public function __construct() {

        $host = 'localhost';
        $user = 'root';
        $password = '';
        $db_name = 'db_cric_24';
        $this->connect = mysqli_connect($host, $user, $password, $db_name);
        if (!$this->connect) {
            die('Error!!!' . mysqli_error($this->connect));
        }
    }
    public function select_all_news_info() {
        $sql="SELECT news_id,news_title,news_type,news_img,DATE_FORMAT(publication_date, '%b-%d-%Y') as publication_date,news_short_desc,popular FROM tbl_add_news";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
    }
    public function select_popular_news() {
        $sql="SELECT news_id,news_title,news_img,DATE_FORMAT(publication_date, '%b-%d-%Y') as publication_date,popular FROM tbl_add_news ORDER BY popular DESC limit 0,5";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
    }
    public function select_latest_news() {
        $sql="SELECT news_id,news_title,news_type,DATE_FORMAT(publication_date, '%b-%d-%Y') as publication_date,popular FROM tbl_add_news WHERE publication_date>='2016-08-10'";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
    }
    public function select_news_by_id($news_id) {
        $sql="SELECT news_title,news_type,DATE_FORMAT(publication_date, '%b-%d-%Y') as publication_date,news_img,news_short_desc,news_long_desc from tbl_add_news where news_id=$news_id";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
    }
    public function update_popular_by_id($news_id,$popular) {
        $sql="update tbl_add_news set popular=$popular+1 where news_id=$news_id";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
        
    }
    public function select_fixture_content() {
        $sql="SELECT tournament_name,country_1,country_2,DATE_FORMAT(match_date, '%b-%d-%Y') as match_date,match_type,match_no,match_time_gmt,match_time_local FROM fixture WHERE match_date>=CURDATE() order by match_date asc limit 0,3";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
    }
    public function select_all_fixture_content() {
        $sql="SELECT tournament_name,country_1,country_2,DATE_FORMAT(match_date, '%b-%d-%Y') as match_date,match_type,match_no,match_time_gmt,match_time_local FROM fixture WHERE match_date>=CURDATE() order by match_date asc ";
        if(mysqli_query($this->connect, $sql)) {
           $query_result=mysqli_query($this->connect, $sql);
           return $query_result;
        }else {
            die('Query problem'.  mysqli_error($this->connect) );   
        }
    }
    public function select_test_ranking() {
        $sql = "SELECT  country_img, country_name,test_played,test_rating, test_rank FROM tbl_ranking order by test_rank asc";
        if (mysqli_query($this->connect, $sql)) {
            $query_result_test = mysqli_query($this->connect, $sql);
            return $query_result_test;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function select_odi_ranking() {
        $sql = "SELECT  country_img, country_name,odi_played,odi_rating, odi_rank FROM tbl_ranking order by odi_rank asc";
        if (mysqli_query($this->connect, $sql)) {
            $query_result_odi = mysqli_query($this->connect, $sql);
            return $query_result_odi;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function select_t20_ranking() {
        $sql = "SELECT  country_img, country_name,t20_played,t20_rating, t20_rank FROM tbl_ranking order by t20_rank asc";
        if (mysqli_query($this->connect, $sql)) {
            $query_result_odi = mysqli_query($this->connect, $sql);
            return $query_result_odi;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    
    public function show_result() {
        $sql="select f.tournament_name,f.country_1,f.country_2,f.match_type,f.match_no,DATE_FORMAT(f.match_date, '%b-%d-%Y') as match_date,r.winning_team,r.win_desc FROM fixture f,tbl_result r where f.fixture_id=r.fixture_id limit 0,3";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function show_live_matches() {
        $sql="select * from fixture where match_date=CURDATE()";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    
    
}
