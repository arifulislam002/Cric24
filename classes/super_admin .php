<?php

class Super_admin {

    private $connect;
    private $directory;
    private $target_file;
    private $file_type;

    public function __construct() {

        $host = 'localhost';
        $user = 'root';
        $password = '';
        $db_name = 'db_cric_24';
        $this->connect = mysqli_connect($host, $user, $password, $db_name);
        if (!$this->connect) {
            die('Error!!!' . mysqli_error($this->connect));
        }
    }

    public function save_news($data) {
        $directory = '../assets/admin_assets/news_images/';
        $target_file = $directory . $_FILES['news_img']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['news_img']['size'];
        $image= getimagesize($_FILES['news_img']['tmp_name']);
        if ($image) {
            if (file_exists($target_file)) {
                echo 'This image already exists';
                exit();
            } else {
                if ($file_size > 5242880) {
                    echo 'File size is too large. Please select a file within 5MB';
                    exit();
                } else {
                    move_uploaded_file($_FILES['news_img']['tmp_name'], $target_file);
                    $sql = "insert into tbl_add_news (news_title,news_type,news_img,news_short_desc,news_long_desc,publication_date) values ('$data[news_title]','$data[news_type]','$target_file','$data[news_short_desc]','$data[news_long_desc]','$data[publication_date]' )";
                    if (mysqli_query($this->connect, $sql)) {
                        $message = "news added successfully";
                        return $message;
                    } else {
                        die('Query problem' . mysqli_error($this->connect));
                    }
                }
            }
        }
    }

    public function select_news() {
        $sql = "SELECT * FROM tbl_add_news";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function select_news_info_by_id($news_id) {
        $sql = "SELECT * FROM tbl_add_news where news_id='$news_id'";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function update_news_info($data) {
        $sql = "UPDATE tbl_add_news SET news_title='$data[news_title]',news_type='$data[news_type]',news_short_desc='$data[news_short_desc]',news_long_desc='$data[news_long_desc]',publication_date='$data[publication_date]' WHERE news_id='$data[news_id]' ";
        if (mysqli_query($this->connect, $sql)) {

            header('Location:manage_news.php');
           
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function delete_news_by_id($news_id){
        $sql = "DELETE FROM tbl_add_news WHERE news_id='$news_id' ";
        if (mysqli_query($this->connect, $sql)) {
            $message = "News deleted successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }

    public function save_ranking($data) {
        $directory = '../assets/admin_assets/country_images/';
        $target_file = $directory . $_FILES['country_img']['name'];
        $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
        $file_size = $_FILES['country_img']['size'];
        $image = getimagesize($_FILES['country_img']['tmp_name']);
        if ($image) {
            if (file_exists($target_file)) {
                echo 'This image already exists';
                exit();
            } else {
                if ($file_size > 5242880) {
                    echo 'File size is too large. Please select a file within 5MB';
                    exit();
                } else {
                    move_uploaded_file($_FILES['country_img']['tmp_name'], $target_file);
                    $sql = "insert into tbl_ranking (country_img,country_name,membership,test_played,test_rating,test_rank,odi_played,odi_rating,odi_rank,t20_played,t20_rating,t20_rank)values('$target_file','$data[country_name]','$data[membership]','$data[test_played]','$data[test_rating]','$data[test_rank]','$data[odi_played]','$data[odi_rating]','$data[odi_rank]','$data[t20_played]','$data[t20_rating]','$data[t20_rank]')";
                    if (mysqli_query($this->connect, $sql)) {
                        $message = "ranking added successfully";
                        return $message;
                    } else {
                        die('Query problem' . mysqli_error($this->connect));
                    }
                }
            }
        }
    }

    public function select_test_ranking() {
        $sql = "SELECT ranking_id, country_img, country_name,membership,test_played,test_rating, test_rank FROM tbl_ranking";
        if (mysqli_query($this->connect, $sql)) {
            $query_result_test = mysqli_query($this->connect, $sql);
            return $query_result_test;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }

    public function select_odi_ranking() {
        $sql = "SELECT ranking_id,country_img, country_name,membership,odi_played,odi_rating, odi_rank FROM tbl_ranking";
        if (mysqli_query($this->connect, $sql)) {
            $query_result_odi = mysqli_query($this->connect, $sql);
            return $query_result_odi;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }

    public function select_t20_ranking() {
        $sql = "SELECT ranking_id,country_img, country_name,membership,t20_played,t20_rating, t20_rank FROM tbl_ranking";
        if (mysqli_query($this->connect, $sql)) {
            $query_result_t20 = mysqli_query($this->connect, $sql);
            return $query_result_t20;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
     public function select_ranking_info($ranking_id) {
        $sql = "SELECT * FROM tbl_ranking where ranking_id='$ranking_id'";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function update_ranking_info($data) {
        $sql = "UPDATE tbl_ranking SET country_name='$data[country_name]',membership='$data[membership]' , test_played='$data[test_played]', test_rating='$data[test_rating]',test_rank='$data[test_rank]',odi_played='$data[odi_played]',odi_rating='$data[odi_rating]',odi_rank='$data[odi_rank]',t20_played='$data[t20_played]',t20_rating='$data[t20_rating]',t20_rank='$data[t20_rank]' WHERE ranking_id='$data[ranking_id]' ";
        if (mysqli_query($this->connect, $sql)) {

            header('Location: manage_ranking.php');
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function delete_ranking_info_by_id($ranking_id){
        $sql = "DELETE FROM tbl_ranking WHERE ranking_id='$ranking_id' ";
        if (mysqli_query($this->connect, $sql)) {
            $message = "Ranking deleted successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function save_fixture($data) {

        $sql = "insert into fixture (tournament_name,country_1,country_2,match_type,match_no,match_date,match_time_gmt,match_time_local)values('$data[tournament_name]','$data[country_1]','$data[country_2]','$data[match_type]','$data[match_no]','$data[match_date]','$data[match_time_gmt]','$data[match_time_local]')";
        if (mysqli_query($this->connect, $sql)) {
            $message = "fixture added successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }

    public function select_fixture() {
        $sql = "SELECT * FROM fixture";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }

    public function select_fixture_info($fixture_id) {
        $sql = "SELECT * FROM fixture where fixture_id='$fixture_id'";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function update_fixture_info($data) {
        $sql = "UPDATE fixture SET tournament_name='$data[tournament_name]',country_1='$data[country_1]' , country_2='$data[country_2]', match_type='$data[match_type]',match_no='$data[match_no]',match_date='$data[match_date]',match_time_gmt='$data[match_time_gmt]',match_time_local='$data[match_time_local]' WHERE fixture_id='$data[fixture_id]' ";
        if (mysqli_query($this->connect, $sql)) {

            header('Location: manage_fixture.php');
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function delete_fixture_by_id($fixture_id){
        $sql = "DELETE FROM fixture WHERE fixture_id='$fixture_id' ";
        if (mysqli_query($this->connect, $sql)) {
            $message = "Fixture deleted successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function save_result($data) {
        $sql="insert into tbl_result(fixture_id,winning_team,win_desc)values('$data[fixture_id]','$data[winning_team]','$data[win_desc]')";
        if (mysqli_query($this->connect, $sql)) {
            $message = "Result added successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function select_result() {
        $sql = "SELECT * FROM tbl_result";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function select_result_info_by_id($result_id) {
        $sql = "SELECT * FROM tbl_result where result_id='$result_id'";
        if (mysqli_query($this->connect, $sql)) {
            $query_result = mysqli_query($this->connect, $sql);
            return $query_result;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function update_result_info($data) {
        $sql = "UPDATE tbl_result SET fixture_id='$data[fixture_id]',winning_team='$data[winning_team]' , win_desc='$data[win_desc]' WHERE result_id='$data[result_id]' ";
        if (mysqli_query($this->connect, $sql)) {

            header('Location: manage_result.php');
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
    public function delete_result_by_id($result_id){
        $sql = "DELETE FROM tbl_result WHERE result_id='$result_id' ";
        if (mysqli_query($this->connect, $sql)) {
            $message = "Result deleted successfully";
            return $message;
        } else {
            die('Query problem' . mysqli_error($this->connect));
        }
    }
   

    public function logout() {
        unset($_SESSION['admin_name']);
        unset($_SESSION['admin_id']);
        header('Location: index.php');
    }

}

?>