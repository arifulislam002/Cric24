<header>
                <div class="col-md-12">
                    <div class="row">
                        <!-- Navigation -->
                        <div class="col-md-12">
                            <div class="menu-trigger"><i class="fa fa-align-justify"></i> Menu</div>
                            <nav>
                                <ul id="nav">
                                    <li class="n-menu"><a href="index.php">Home</a> </li>

                                    <li id="selected" class="n-menu subnav">
								<a href="" class="sub-nav">Rankings</a>
								<ul class="mega-menu simple">
									<li class="sub-menu">
										<ul>
											<li><a href="test.php">Test</a></li>
											<li><a href="odi.php">ODI</a></li>
											<li><a href="t20.php">T-20</a></li>
											
										</ul>
									</li>
								</ul>
							</li>
                                    
                                    <li class="n-menu"><a href="fixtures.php">Fixtures</a></li>
                                    <li class="n-menu"><a href="live_matches.php">Live Matches</a></li>
                                    <li class="n-menu"><a href="results.php">Results</a></li>
                                    <li class="n-menu"><a href="gallery.php">Gallery</a></li>
                                    <li class="n-menu"><a href="contact.php">Contact us</a></li>
                                </ul>
                            </nav>

                            <!-- Search -->
                            <div class="search">
                                <form>
                                    <input type="search" placeholder="Type to search and hit enter">
                                </form>
                            </div>
                            <span class="search-trigger"><i class="fa fa-search"></i></span>
                        </div>
                    </div>
                </div>
            </header>