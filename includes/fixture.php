<?php
$query_result = $obj_app->select_fixture_content();

?>
<div class="side-widget p-news">
    <h5><span>Fixtures</span></h5>
    <?php while ($fixture_info = mysqli_fetch_assoc($query_result)) { ?>

        <div class="bla-content">
            <span class="cat-default"><h6><?php echo $fixture_info['tournament_name'].'&nbsp;&nbsp'?><?php echo ' || '; ?><?php echo '&nbsp;&nbsp'.$fixture_info['country_1'].'&nbsp;&nbsp' ?><?php echo '--VS--'; ?><?php echo '&nbsp;&nbsp'.$fixture_info['country_2'].'&nbsp;&nbsp'; ?><?php echo '||'; ?><?php echo '&nbsp;&nbsp'.$fixture_info['match_no']; ?><?php echo '&nbsp;&nbsp'.$fixture_info['match_type']; ?></h6></span>
            <p><h6><i class="fa fa-clock-o"></i> <?php echo $fixture_info['match_date'].'&nbsp;&nbsp'.'||'.'&nbsp;&nbsp'.$fixture_info['match_time_gmt'].'(GMT)'.'&nbsp;&nbsp'.'||'.'&nbsp;&nbsp'.$fixture_info['match_time_local'].'(BST)'; ?></h6></p>
            
            <div class="sep"></div>

        </div>
    <?php } ?>

</div>