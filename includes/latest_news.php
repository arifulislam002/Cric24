<?php
$query_result = $obj_app->select_latest_news();
?>
<div class="col-md-3 b1-aside">
    <h5><span>Latest News</span></h5>
    <?php while ($news_info=  mysqli_fetch_assoc($query_result)){ ?>
    <div class="bla-content">
        <span class="cat-default"><?php echo $news_info['news_type']; ?></span>
        <p><i class="fa fa-clock-o"></i> <?php echo $news_info['publication_date']; ?></p>
        <h4><a href="news.php?id=<?php echo $news_info['news_id'] ?>&popular=<?php echo $news_info['popular']; ?>"><?php echo $news_info['news_title']; ?></a></h4>
        <div class="sep"></div>

    </div>
 <?php } ?>
</div>