<?php
$query_result = $obj_app->show_result();
?>
<div class="side-widget p-news">
    <h5><span>Results</span></h5>
    <?php while ($result_info = mysqli_fetch_assoc($query_result)) { ?>
        <div class="bla-content">
            <span class="cat-default"><h6><?php echo $result_info['tournament_name'].'&nbsp;&nbsp||&nbsp;&nbsp;'.$result_info['country_1'].'&nbsp;&nbsp;VS&nbsp;&nbsp'.$result_info['country_2'].'&nbsp;&nbsp;'.$result_info['match_no'].'&nbsp;&nbsp;'.$result_info['match_type'].'</br>'; ?></h6></span>

            <h6><?php echo $result_info['winning_team'].'&nbsp;&nbsp;'.$result_info['win_desc']; ?></h6>
            <div class="sep"></div>

        </div>
    <?php } ?>

</div>