<div class="top-bar container">
    <div class="row">
        <div class="col-md-6">
            <ul class="tb-left">
                <li class="tbl-date"><span>
                        <?php
                        echo "<b>".date('l\, F jS\, Y ')."</b>";
                        ?>
                    </span></li>
                <li class="tbl-temp"><i class="fa fa-sun-o"></i>32&deg; C</li>
            </ul>
        </div>
        <div class="col-md-6">
            <ul class="tb-right">
                <li class="tbr-social">
                    <span>
                        <a href="#" class="fa fa-twitter"></a>
                        <a href="#" class="fa fa-facebook"></a>
                        <a href="#" class="fa fa-google-plus"></a>
                        <a href="#" class="fa fa-rss"></a>
                        <a href="#" class="fa fa-pinterest"></a>
                        <a href="#" class="fa fa-youtube-play"></a>
                    </span>
                </li>
                <li class="tbr-login">
                    <a href="login.html">Login</a>
                </li>
            </ul>
        </div>
    </div>
</div>
