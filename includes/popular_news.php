<?php
$query_result = $obj_app->select_popular_news();
?>
<div class="side-widget p-news">
                        <h5><span>Popular news</span></h5>
                        <div class="sw-inner">
                            <ul>
                                <?php while ($news_info=  mysqli_fetch_assoc($query_result)){ ?>
                                <li>
                                    <img src="pages/<?php echo $news_info['news_img']; ?>" alt=""/>
                                    <div class="pn-info">
                                        <em><i class="fa fa-clock-o"></i> <?php echo  $news_info['publication_date']; ?> </em>
                                        <h4><a href="news.php?id=<?php echo $news_info['news_id'] ?>&popular=<?php echo $news_info['popular']; ?>"><?php echo $news_info['news_title']; ?></a></h4>
                                    </div>
                                </li>
                                
                                
                                
                                <?php } ?>
                            </ul>
                        </div>
                    </div>