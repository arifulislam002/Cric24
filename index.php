<?php
require './classes/application.php';
$obj_app=new Application();
?>
<!DOCTYPE html>
<html> 
    <head>

        <!-- Meta -->
        <meta charset="utf-8">
        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Cric24 || Always with Cricket</title>

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Favicons -->
        <link rel="shortcut icon" href="assets/front_end_assets/img/favicon.ico">
        <link rel="shortcut icon" href="http://example.com/favicon.ico" />

        <!-- Google Fonts & Fontawesome -->
        <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
        <link href="../../../../maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,900,800' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Josefin+Sans:400,100,300,300italic,100italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,100,300,300italic,100italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>

        <!-- CSS -->
        <link rel="stylesheet" href="assets/front_end_assets/css/bootstrap.css">
        <link rel="stylesheet" href="assets/front_end_assets/js/vendor/slick/slick.css">
        <link rel="stylesheet" href="assets/front_end_assets/css/prettyphoto.css">
        <link rel="stylesheet" href="assets/front_end_assets/css/style.css">

        
        <script src="assets/front_end_assets/js/css3-mediaqueries.js"></script>

    </head>
    <body>

        <!-- Topbar -->
        <?php include './includes/top_bar.php'; ?>

        <div class="container wrapper">
            <!--Banner-->
            <?php include './includes/banner.php'; ?>
            <!-- Header -->
            <?php include './includes/header.php'; ?>

            <!-- Main Content -->
            <div class="main-content container">
                <div class="col-md-8 block-1">
                    <div class="row">
                      <!---latest news -->
                      <?php include './includes/latest_news.php';?>

                        <div class="col-md-9 block-right">
                            <?php
                            if(isset($pages)){
                                if($pages=='test'){
                                    include './pages/test_ranking_content.php';
                                }
                            elseif ($pages=='odi') {
                                     include './pages/odi_ranking_content.php';
                                
                            }
                            elseif ($pages=='t20') {
                                     include './pages/t20_ranking_content.php';
                                
                            }
                            elseif ($pages=='results') {
                                    include './pages/result_content.php';
                                
                            }
                             elseif ($pages=='gallery') {
                                    include './pages/gallery_contentt.php';
                                
                            }   
                            elseif ($pages=='live_matches') {
                                    include './pages/live_matches_content.php';
                                
                            }   
                            elseif ($pages=='contact') {
                                    include './pages/contact_content.php';
                                
                            } 
                             elseif ($pages=='news') {
                                    include './pages/news_content.php';
                                
                            }
                              elseif ($pages=='fixtures') {
                                    include './pages/fixture_content.php';
                                
                            }
                            }
                            else {
                                
                                include './pages/responsive_images.php';
                                
                                include './pages/featured_news.php'; 
                           
                            }
                            ?>
                            

                  
                        </div>
                    </div>
                </div>

                <!-- Sidebar -->
                <aside class="col-md-4">
                    <?php include './includes/popular_news.php';?>
                    <?php include './includes/fixture.php';?>
                    <?php include './includes/result.php';?>
                    
                    <!-- Popular News -->
                    <!-- Banner -->
                    <!--most commented-->
                 
                </aside>
            </div>

            <!-- Footer -->

            <?php include './includes/footer.php'; ?>
      
        </div>

        <div class="clearfix space30"></div>

        <!-- Javascript -->
        <script src="assets/front_end_assets/js/jquery.min.js"></script>
        <script src="assets/front_end_assets/js/bootstrap.min.js"></script>
        <script src="assets/front_end_assets/js/vendor/slick/slick.js"></script>
        <script src="assets/front_end_assets/js/jquery.nicescroll.js"></script>
        <script src="assets/front_end_assets/js/jflickrfeed.min.js"></script>
        <script src="assets/front_end_assets/js/jquery-ui.js"></script>
        <script src="assets/front_end_assets/js/jquery.spasticNav.js"></script>
        <script src="assets/front_end_assets/js/jquery.prettyphoto.js"></script>
        <script src="assets/front_end_assets/js/main.js"></script>

    </body>

    <!-- Mirrored from premiumlayers.com/demos/gazeta2/gazeta/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 29 Jul 2016 09:26:15 GMT -->
</html>

