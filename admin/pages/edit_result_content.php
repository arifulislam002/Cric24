<?php
$result_id=$_GET['id'];
$query_result=$obj_sup_admin->select_result_info_by_id($result_id);
$result_info=mysqli_fetch_assoc($query_result);

if(isset($_POST['btn'])) {
    $obj_sup_admin->update_result_info($_POST);
}

?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Results</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                       <div class="control-group">
                        <label class="control-label" for="typeahead">Fixture ID</label>
                        <div class="controls">
                            <input type="hidden" name="result_id" class="span6 typeahead" id="typeahead" value="<?php echo $result_info['result_id']; ?>" >
                            <input type="text" name="fixture_id" class="span6 typeahead" id="typeahead" value="<?php echo $result_info['fixture_id']; ?>">

                        </div>
                    </div> 
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Winning Team</label>
                        <div class="controls">
                            <select id="selectError3" name="winning_team">
                                <option><?php echo $result_info['winning_team']; ?></option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>
                                
                                
                            </select>
                        </div>
                    </div>

                     <div class="control-group">
                        <label class="control-label" for="typeahead">Win Description</label>
                        <div class="controls">
                            <input type="text" name="win_desc" class="span6 typeahead" id="typeahead" value="<?php echo $result_info['win_desc']; ?>">

                        </div>
                    </div> 
                    
                    
                    
 
                 
                    </div> 

                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Save Changes</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
