<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Live Matches</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                       <div class="control-group">
                        <label class="control-label" for="typeahead">Tournament Name </label>
                        <div class="controls">
                            <input type="text" name="country_img_1" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                           <div class="control-group">
                        <label class="control-label" for="typeahead">Country Image </label>
                        <div class="controls">
                            <input type="file" name="country_img_1" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                    
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Country Name</label>
                        <div class="controls">
                            <select id="selectError3" name="country_1">
                                <option>---Select country---</option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>
                                
                            </select>
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label" for="typeahead">VS--Country Image </label>
                        <div class="controls">
                            <input type="file" name="country_img_2" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                    
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Country Name</label>
                        <div class="controls">
                            <select id="selectError3" name="country_2">
                                <option>---Select country---</option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>
                                
                            </select>
                        </div>
                    </div>
                     <div class="control-group">
                        <label class="control-label" for="selectError3">Game Type</label>
                        <div class="controls">
                            <select id="selectError3" name="country_2">
                                <option>---Select country---</option>
                                <option>Test</option>
                                <option>ODI</option>
                                <option>T20</option>
                               
                                
                            </select>
                        </div>
                    </div>
 
                 
                    </div> 

                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
