<?php
$query_result_test=$obj_sup_admin->select_test_ranking();
$query_result_odi=$obj_sup_admin->select_odi_ranking();
$query_result_t20=$obj_sup_admin->select_t20_ranking();
if (isset($_GET['status'])){
    $ranking_id=$_GET['id'];
    if($_GET['status']=='delete'){
        $message=$obj_sup_admin->delete_ranking_info_by_id($ranking_id);
    }
}
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Test Ranking</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2>
            <?php
                if(isset($meassage)) {
                    echo $meassage;
                }
                unset($meassage);
            ?>
            </h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Name</th>
                        <th>Membership</th>
                        <th>Test Matches</th>
                        <th>Rating</th>
                        <th>Rank</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($ranking_info=mysqli_fetch_assoc($query_result_test)) { ?>
                    <tr>
                        <td class="center"><img src="<?php echo $ranking_info['country_img'];?>" height="100" width="100"></td>
                        <td class="center"><?php echo $ranking_info['country_name']; ?></td>
                        <td class="center"><?php echo $ranking_info['membership']; ?></td>
                        <td class="center"><?php echo $ranking_info['test_played']; ?></td>
                        <td class="center"><?php echo $ranking_info['test_rating']; ?></td>
                        <td class="center"><?php echo $ranking_info['test_rank']; ?></td>
                       
                        <td class="center">
                            <a class="btn btn-info" href="edit_ranking.php?id=<?php echo $ranking_info['ranking_id']; ?>" title="Edit">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="?status=delete&&id=<?php echo $ranking_info['ranking_id']; ?>" onclick="return check_delete_info()">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
        
    <div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>ODI Ranking</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Name</th>
                        <th>Membership</th>
                        <th>ODI Matches</th>
                        <th>Rating</th>
                        <th>Rank</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($ranking_info=mysqli_fetch_assoc($query_result_odi)) { ?>
                    <tr>
                        <td class="center"><img src="<?php echo $ranking_info['country_img']; ?>" height="100" width="100"></td>
                        <td class="center"><?php echo $ranking_info['country_name']; ?></td>
                        <td class="center"><?php echo $ranking_info['membership']; ?></td>
                        <td class="center"><?php echo $ranking_info['odi_played']; ?></td>
                        <td class="center"><?php echo $ranking_info['odi_rating']; ?></td>
                        <td class="center"><?php echo $ranking_info['odi_rank']; ?></td>
                       
                        <td class="center">
                            
                            <a class="btn btn-info" href="edit_ranking.php?id=<?php echo $ranking_info['ranking_id']; ?>" title="Edit" title="Edit">
                                <i class="halflings-icon white edit" ></i>  
                            </a>
                            <a class="btn btn-danger" href="?status=delete&&id=<?php echo $ranking_info['ranking_id']; ?>" onclick="return check_delete_info()">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
        <div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>T20 Ranking</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Country</th>
                        <th>Name</th>
                        <th>Membership</th>
                        <th>T20 Matches</th>
                        <th>Rating</th>
                        <th>Rank</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($ranking_info=mysqli_fetch_assoc($query_result_t20)) { ?>
                    <tr>
                        <td class="center"><img src="<?php echo $ranking_info['country_img']; ?>" height="100" width="100"></td>
                        <td class="center"><?php echo $ranking_info['country_name']; ?></td>
                        <td class="center"><?php echo $ranking_info['membership']; ?></td>
                        <td class="center"><?php echo $ranking_info['t20_played']; ?></td>
                        <td class="center"><?php echo $ranking_info['t20_rating']; ?></td>
                        <td class="center"><?php echo $ranking_info['t20_rank']; ?></td>
                       
                        <td class="center">
                           
                            <a class="btn btn-info" href="edit_ranking.php?id=<?php echo $ranking_info['ranking_id']; ?>" title="Edit">
                                <i class="halflings-icon white edit" ></i>  
                            </a>
                            <a class="btn btn-danger" href="?status=delete&&id=<?php echo $ranking_info['ranking_id']; ?>" onclick="return check_delete_info()">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
<script type="text/javascript">
   function check_delete_info(){
        var msg=confirm('Are You Sure to Delete This!');
    if(msg)
    {
        return true;
    }
    else{
        return false;
    }
   }
</script>