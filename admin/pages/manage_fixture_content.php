<?php
$query_result = $obj_sup_admin->select_fixture();
if (isset($_GET['status'])){
    $fixture_id=$_GET['id'];
    if($_GET['status']=='delete'){
        $message=$obj_sup_admin->delete_fixture_by_id($fixture_id);
    }
}
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Fixture</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Fixture ID</th>
                        <th>Tournament</th>
                        <th>Team 1</th>
                        <th>Team 2</th>
                        <th>Match Type</th>
                        <th>Match No.</th>
                        <th>Match Date</th>
                        <th>Time(GMT)</th>
                        <th>Time(BST)</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($fixture_info = mysqli_fetch_assoc($query_result)) { ?>
                        <tr>


                            <td class="center"><?php echo $fixture_info['fixture_id']; ?></td>
                            <td class="center"><?php echo $fixture_info['tournament_name']; ?></td>
                            <td class="center"><?php echo $fixture_info['country_1']; ?></td>
                            <td class="center"><?php echo $fixture_info['country_2']; ?></td>
                            <td class="center"><?php echo $fixture_info['match_type']; ?></td>
                            <td class="center"><?php echo $fixture_info['match_no']; ?></td>
                            <td class="center"><?php echo $fixture_info['match_date']; ?></td>
                            <td class="center"><?php echo $fixture_info['match_time_gmt']; ?></td>
                            <td class="center"><?php echo $fixture_info['match_time_local']; ?></td>
                            <td class="center">

                                <a class="btn btn-info" href="edit_fixture.php?id=<?php echo $fixture_info['fixture_id']; ?>" title="Edit">

                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?status=delete&&id=<?php echo $fixture_info['fixture_id']; ?>" title="Delete" onclick="check_delete_info()">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
        <script type="text/javascript">
            function check_delete_info(){
                 var msg=confirm('Are You Sure to Delete This!');
             if(msg)
             {
                 return true;
             }
             else{
                 return false;
             }
            }
        </script>