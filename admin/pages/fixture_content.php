<?php
if (isset($_POST['btn'])){
$message=$obj_sup_admin->save_fixture($_POST);
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Fixtures</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>

        <div class="box-content">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                       <div class="control-group">
                        <label class="control-label" for="typeahead">Tournament Name </label>
                        <div class="controls">
                            <input type="text" name="tournament_name" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                    
                    
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Team 1</label>
                        <div class="controls">
                            <select id="selectError3" name="country_1">
                                <option>---Select country---</option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>
                                
                                
                            </select>
                        </div>
                    </div>
                   
                    <h2 style="text-align:left">-------------------------------------VS-----------------------------------</h2>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Team 2</label>
                        <div class="controls">
                            <select id="selectError3" name="country_2">
                                <option>---Select country---</option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>
                                
                            </select>
                        </div>
                    </div>
                    <h2 style="text-align:left">-----------------------------------------------------------------------------</h2>

                     <div class="control-group">
                        <label class="control-label" for="selectError3">Match Type</label>
                        <div class="controls">
                            <select id="selectError3" name="match_type">
                                <option>---Select Type---</option>
                                <option>Test</option>
                                <option>ODI</option>
                                <option>T20</option>
                               
                                
                            </select>
                        </div>
                     <div class="control-group">
                        <label class="control-label" for="selectError3">Match No.</label>
                        <div class="controls">
                            <select id="selectError3" name="match_no">
                                <option>---Select---</option>
                                <option>1st</option>
                                <option>2nd</option>
                                <option>3rd</option>
                                <option>4th</option>
                                <option>5th</option>
                                <option>6th</option>
                                <option>7th</option>
                                <option>8th</option>
                                <option>9th</option>
                                <option>10th</option>
                                <option>11th</option>
                                <option>12th</option>
                                <option>Only</option>
                                
                            </select>
                        </div>
                    </div>    
                    </div>
                     <div class="control-group">
                        <label class="control-label" for="typeahead">Match Day</label>
                        <div class="controls">
                            <input type="date" name="match_date" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Time(GMT)</label>
                        <div class="controls">
                            <input type="time" name="match_time_gmt" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Local Time(BST)</label>
                        <div class="controls">
                            <input type="time" name="match_time_local" class="span6 typeahead" id="typeahead" >

                        </div>
                    </div> 
                    
 
                 
                    </div> 

                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Save</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
