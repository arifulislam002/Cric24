<?php
$news_id=$_GET['id'];
$query_result=$obj_sup_admin->select_news_info_by_id($news_id);
$news_info=  mysqli_fetch_assoc($query_result);

if (isset($_POST['btn'])){
$obj_sup_admin->update_news_info($_POST);
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update News</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <?php if(isset($message)){
                        echo $message;}
                        unset($message)?>
        <div class="box-content">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">News Title </label>
                        <div class="controls">
                            <input type="hidden" name="news_id" class="span6 typeahead" id="typeahead" value="<?php echo $news_info['news_id'];?>">
                            <input type="text" name="news_title" class="span6 typeahead" id="typeahead" value="<?php echo $news_info['news_title'];?>" >
                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="selectError3">News Type</label>
                        <div class="controls">
                            <select id="selectError3" name="news_type">
                                <option><?php echo $news_info['news_type'];?></option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>
                                
                            </select>
                        </div>
                    </div>
                   
                    </div> 
                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">News Short Description</label>
                        <div class="controls">
                            <textarea  name="news_short_desc"  rows="3" ><?php echo $news_info['news_short_desc'];?></textarea>
                        </div>
                    </div>
                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">News Full Description</label>
                        <div class="controls">
                            <textarea  name="news_long_desc"  rows="3"><?php echo $news_info['news_long_desc'];?></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Publication Date</label>
                        <div class="controls">
                            
                            <input type="date" name="publication_date" class="span6 typeahead" id="typeahead" value="<?php echo $news_info['publication_date'];?>" >
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Save Changes</button>
                        <button type="reset" name="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div>