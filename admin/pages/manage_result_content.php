<?php
$query_result = $obj_sup_admin->select_result();
if (isset($_GET['status'])){
    $result_id=$_GET['id'];
    if($_GET['status']=='delete'){
        $message=$obj_sup_admin->delete_result_by_id($result_id);
    }
}
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Fixture</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Fixture ID</th>
                        <th>Winning Team</th>
                        <th>Desc</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($result_info = mysqli_fetch_assoc($query_result)) { ?>
                        <tr>


                            <td class="center"><?php echo $result_info['fixture_id']; ?></td>
                            <td class="center"><?php echo $result_info['winning_team']; ?></td>
                            <td class="center"><?php echo $result_info['win_desc']; ?></td>
                          
                            <td class="center">

                                <a class="btn btn-info" href="edit_result.php?id=<?php echo $result_info['result_id']; ?>" title="Edit">

                                    <i class="halflings-icon white edit"></i>  
                                </a>
                                <a class="btn btn-danger" href="?status=delete&id=<?php echo $result_info['result_id']; ?>" title="Delete" onclick="check_delete_info()">
                                    <i class="halflings-icon white trash"></i> 
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
        <script type="text/javascript">
            function check_delete_info(){
                 var msg=confirm('Are You Sure to Delete This!');
             if(msg)
             {
                 return true;
             }
             else{
                 return false;
             }
            }
        </script>