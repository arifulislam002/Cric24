<?php
$ranking_id=$_GET['id'];
$query_result=$obj_sup_admin->select_ranking_info($ranking_id);
$ranking_info=  mysqli_fetch_assoc($query_result);

if(isset($_POST['btn'])){
    $obj_sup_admin->update_ranking_info($_POST);
}
?>
<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Ranking Update Form</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
<?php
if (isset($message)) {
    echo $message;
}
unset($message);
?>
        <div class="box-content">
            <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                <fieldset>


                    <div class="control-group">
                        <input type="hidden" name="ranking_id" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['ranking_id']; ?>" >
                        <label class="control-label" for="selectError3">Country Name</label>
                        <div class="controls">
                            <select id="selectError3" name="country_name">
                                <option><?php echo $ranking_info['country_name']; ?></option>
                                <option>Afghanistan</option>
                                <option>Australia</option>
                                <option>Bangladesh</option>
                                <option>England</option>
                                <option>India</option>
                                <option>Ireland</option>
                                <option>New Zealand</option>
                                <option>Pakistan</option>
                                <option>Sri Lanka</option>
                                <option>South Africa</option>
                                <option>West Indies</option>
                                <option>Zimbabwe</option>

                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Membership</label>
                        <div class="controls">
                            <select id="selectError3" name="membership">
                                <option><?php echo $ranking_info['membership']; ?></option>
                                <option>Full member</option>
                                <option>Associate</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Test Played </label>
                        <div class="controls">
                            <input type="text" name="test_played" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['test_played']; ?>" >

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Test Rating </label>
                        <div class="controls">
                            <input type="text" name="test_rating" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['test_rating']; ?>">

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="selectError3">Test Ranking</label>
                        <div class="controls">
                            <select id="selectError3" name="test_rank">
                                <option><?php echo $ranking_info['test_rank']; ?></option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>


                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">ODI Played </label>
                        <div class="controls">
                            <input type="text" name="odi_played" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['odi_played']; ?>" >

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">ODI Rating </label>
                        <div class="controls">
                            <input type="text" name="odi_rating" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['odi_rating']; ?>">

                        </div>
                    </div> 

                    <div class="control-group">
                        <label class="control-label" for="selectError3">ODI Ranking</label>
                        <div class="controls">
                            <select id="selectError3" name="odi_rank">
                                <option><?php echo $ranking_info['odi_rank']; ?></option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                                <option>15</option>

                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">T20 Played </label>
                        <div class="controls">
                            <input type="text" name="t20_played" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['t20_played']; ?>" >

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="typeahead">T20 Rating </label>
                        <div class="controls">
                            <input type="text" name="t20_rating" class="span6 typeahead" id="typeahead" value="<?php echo $ranking_info['t20_rating']; ?>" >

                        </div>
                    </div> 
                    <div class="control-group">
                        <label class="control-label" for="selectError3">T20 Ranking</label>
                        <div class="controls">
                            <select id="selectError3" name="t20_rank">
                                <option><?php echo $ranking_info['t20_rank']; ?></option>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                                <option>15</option>

                            </select>
                        </div>
                    </div>

                    </div> 



                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Save Changes</button>
                        <button type="reset" name="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
