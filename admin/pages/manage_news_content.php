<?php
$query_result=$obj_sup_admin->select_news();
if (isset($_GET['status'])){
    $news_id=$_GET['id'];
    if($_GET['status']== 'delete'){
        $message=$obj_sup_admin->delete_news_by_id($news_id);
    }
}
?>
<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <h2>
            <?php
                if(isset($meassage)) {
                    echo $meassage;
                     unset($meassage);
                }
               
            ?>
            </h2>
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>News ID</th>
                        <th>News Title</th>
                        <th>News Type</th>
                        <th>Image</th>
                        <th>Short Desc</th>
                        <th>Long Desc</th>
                        <th>Date</th>
                        <th>popularity</th>
                        <th>Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php while ($news_info=mysqli_fetch_assoc($query_result)) { ?>
                    <tr>
                        
                        
                        <td class="center"><?php echo $news_info['news_id'];?></td>
                        <td class="center"><?php echo $news_info['news_title'];?></td>
                        <td class="center"><?php echo $news_info['news_type'];?></td>
                        <td class="center"><img src="<?php echo $news_info['news_img']; ?>"   height="100" width="100"></td>
                        <td class="center"><?php echo $news_info['news_short_desc'];?></td>
                        <td class="center"><?php echo $news_info['news_long_desc'];?></td>
                        <td class="center"><?php echo $news_info['publication_date'];?></td>
                        <td class="center"><?php echo $news_info['popular'];?></td>
                       
                        <td class="center">
                           
                            <a class="btn btn-info" href="edit_news.php?id=<?php echo $news_info['news_id'];?>" title="Edit">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="?status=delete&&id=<?php echo $news_info['news_id'];?>" title="Delete" onclick="return check_delete_info();">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>
<script type="text/javascript">
   function check_delete_info(){
        var msg=confirm('Are You Sure to Delete This!');
    if(msg)
    {
        return true;
    }
    else{
        return false;
    }
   }
</script>