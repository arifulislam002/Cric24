<?php
    session_start();
     
     $admin_id=  isset($_SESSION['admin_id']);
     if($admin_id){
         header('location:admin_master.php');
     }
     
     
    if(isset($_POST['btn'])){
        
       require '../classes/admin.php';
       $obj_admin= new Admin();
       $msg=$obj_admin->admin_login_check($_POST);
       
       
         
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>

        <!-- start: Meta -->
        <meta charset="utf-8">
        <title>Cric 24 || Admin Login</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
        <!-- end: Meta -->

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->

        <!-- start: CSS -->
        <link id="bootstrap-style" href="../assets/admin_assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="../assets/admin_assets/css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="../assets/admin_assets/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="../assets/admin_assets/css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <!-- end: CSS -->


        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.assets/admin_assets/js"></script>
                <link id="ie-style" href="css/ie.css" rel="stylesheet">
        <![endif]-->

        <!--[if IE 9]>
                <link id="ie9style" href="css/ie9.css" rel="stylesheet">
        <![endif]-->

        <!-- start: Favicon -->
        <link rel="shortcut icon" href="../assets/admin_assets/img/favicon.ico">
        <!-- end: Favicon -->

        <style type="text/css">
            body { background: url(../assets/admin_assets/img/bg-login.jpg) !important; }
        </style>



    </head>

    <body>
        
        <div class="container-fluid-full">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="login-box">
                        <div class="icons">
                            <a href="index.html"><i class="halflings-icon home"></i></a>
                            <a href="#"><i class="halflings-icon cog"></i></a>
                        </div>
                        <h2>Login to your account</h2>
                        <h2 style="color:red"><?php if(isset($msg)){echo $msg;} ?></h2>
                        <form class="form-horizontal" action="" method="post">
                            <fieldset>

                                <div class="input-prepend" title="Username">
                                    <span class="add-on"><i class="halflings-icon user"></i></span>
                                    <input class="input-large span10" name="email_address" id="username" type="text" placeholder="type email address"/>
                                </div>
                                <div class="clearfix"></div>

                                <div class="input-prepend" title="Password">
                                    <span class="add-on"><i class="halflings-icon lock"></i></span>
                                    <input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
                                </div>
                                <div class="clearfix"></div>

                                <label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>

                                <div class="button-login">	
                                    <button type="submit" name="btn" class="btn btn-primary">Sign in</button>
                                </div>
                                <div class="clearfix"></div>
                            </fieldset>
                        </form>
                        <hr>
                        <h3>Forgot Password?</h3>
                        <p>
                            No problem, <a href="#">click here</a> to get a new password.
                        </p>	
                    </div><!--/span-->
                </div><!--/row-->


            </div><!--/.fluid-container-->

        </div><!--/fluid-row-->

        <!-- start: JavaScript-->

        <script src="assets/admin_assets/assets/admin_assets/js/jquery-1.9.1.min.assets/admin_assets/js"></script>
        <script src="assets/admin_assets/js/jquery-migrate-1.0.0.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery-ui-1.10.0.custom.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.ui.touch-punch.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/modernizr.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/bootstrap.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.cookie.assets/admin_assets/js"></script>

        <script src='assets/admin_assets/js/fullcalendar.min.assets/admin_assets/js'></script>

        <script src='assets/admin_assets/js/jquery.dataTables.min.assets/admin_assets/js'></script>

        <script src="assets/admin_assets/js/excanvas.assets/admin_assets/js"></script>
        <script src="assets/admin_assets/js/jquery.flot.assets/admin_assets/js"></script>
        <script src="assets/admin_assets/js/jquery.flot.pie.assets/admin_assets/js"></script>
        <script src="assets/admin_assets/js/jquery.flot.stack.assets/admin_assets/js"></script>
        <script src="assets/admin_assets/js/jquery.flot.resize.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.chosen.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.uniform.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.cleditor.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.noty.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.elfinder.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.raty.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.iphone.toggle.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.uploadify-3.1.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.gritter.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.imagesloaded.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.masonry.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.knob.modified.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/jquery.sparkline.min.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/counter.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/retina.assets/admin_assets/js"></script>

        <script src="assets/admin_assets/js/custom.assets/admin_assets/js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
