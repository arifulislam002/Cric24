<div class="side-widget p-news">
    <h5><span>Popular news</span></h5>
    <div class="sw-inner">
        <ul>
            <li>
                <img src="assets/front_end_assets/images/aside/1.jpg" alt=""/>
                <div class="pn-info">
                    <em><i class="fa fa-clock-o"></i> December 13,2014 <a href="#"><i class="fa fa-comments"></i> 5</a></em>
                    <h4><a href="news_single.html">Driverless cars need to make their passengers feel like drivers</a></h4>
                </div>
            </li>
            <li>
                <img src="assets/front_end_assets/images/aside/2.jpg" alt=""/>
                <div class="pn-info">
                    <em><i class="fa fa-clock-o"></i> December 13,2014 <a href="#"><i class="fa fa-comments"></i> 5</a></em>
                    <h4><a href="news_single.html">One of the best phones around: Sony's Xperia Z3 reviewed</a></h4>
                </div>
            </li>
            <li>
                <img src="assets/front_end_assets/images/aside/3.jpg" alt=""/>
                <div class="pn-info">
                    <em><i class="fa fa-clock-o"></i> December 13,2014 <a href="#"><i class="fa fa-comments"></i> 5</a></em>
                    <h4><a href="news_single.html">Indian construction major seeks $803M over scrapped airport deal</a></h4>
                </div>
            </li>
            <li>
                <img src="assets/front_end_assets/images/aside/4.jpg" alt=""/>
                <div class="pn-info">
                    <em><i class="fa fa-clock-o"></i> December 13,2014 <a href="#"><i class="fa fa-comments"></i> 5</a></em>
                    <h4><a href="news_single.html">Qatar Airways, the global launch customer of the new Airbus A350</a></h4>
                </div>
            </li>
            <li>
                <img src="assets/front_end_assets/images/aside/5.jpg" alt=""/>
                <div class="pn-info">
                    <em><i class="fa fa-clock-o"></i> December 13,2014 <a href="#"><i class="fa fa-comments"></i> 5</a></em>
                    <h4><a href="news_single.html">US president's armored car and gum-chewing drew lots of attention</a></h4>
                </div>
            </li>
        </ul>
    </div>
</div>
<div class="side-widget sw-banner">
    <a href="#"><img src="assets/front_end_assets/images/banner/3.jpg" class="img-responsive" alt=""/></a>
</div>