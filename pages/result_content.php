<?php
$query_result=$obj_app->show_result();
?>
 <div class="featured-news">
    <h5><span>Results</span></h5>
<table width="100%" height="auto" border="10px">
    
    <tr>
        <th>Date</th>
        <th>Tournament</th>
        <th>Match</th>
        <th>Result</th>
    </tr>
    <?php while ($result_info=mysqli_fetch_assoc($query_result)){ ?>
    <tr>
        <td><h5><?php echo $result_info['match_date'];?></h5></td>
        <td><h5><?php echo $result_info['tournament_name'];?></h5></td>
        <td><h5><?php echo $result_info['country_1'].'&nbsp;&nbsp;VS&nbsp;&nbsp'.$result_info['country_2'].'&nbsp;&nbsp;'.$result_info['match_no'].'&nbsp;&nbsp;'.$result_info['match_type'];?></h5></td>
        <td><h5><?php echo $result_info['winning_team'].'&nbsp;&nbsp;'.$result_info['win_desc']; ?></h5></td>
    </tr>
    <?php } ?>
</table>
 </div>