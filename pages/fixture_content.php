<?php 
    $query_result=$obj_app->select_all_fixture_content();
?>

<div class="featured-news">
    <h5><span>Fixtures</span></h5>
    
    <table width="100%" height="auto" border="10px">
        <tr>
            <th>Date</th>
            <th>Tournament</th>
            <th>Match</th>
            <th>Starts At</th>
        </tr>
        <?php while ($fixture_info=mysqli_fetch_assoc($query_result)){ ?>
        <tr>
            <td><?php echo $fixture_info['match_date'];?></td>
            <td><?php echo $fixture_info['tournament_name'];?></td>
            <td><?php echo $fixture_info['country_1'].'&nbsp;&nbsp;VS&nbsp;&nbsp'.$fixture_info['country_2'].'&nbsp;&nbsp;'.$fixture_info['match_no'].'&nbsp;&nbsp;'.$fixture_info['match_type'];?></td>
            <td><?php echo $fixture_info['match_time_gmt'].'(GMT)'.'</br>'.$fixture_info['match_time_local'].'(Local)';?></td>
        </tr>
         <?php } ?>
    </table>
 </div>