<?php
$query_result = $obj_app->select_all_news_info();
?>
<div class="featured-news">
    <h5><span>Featured News</span></h5>
    <div class="row">
        <?php while($news_info=mysqli_fetch_assoc($query_result)) {?>
        <div class="col-md-6">
            
            
            <div class="fn-inner">
                <div class="fn-thumb">
                    <img src="pages/<?php echo $news_info['news_img']; ?>"  class="img-responsive" alt=""/>
                    <div class="fn-meta"><?php echo $news_info['news_type']; ?> </div>
                </div>
                <h4><a href="news.php?id=<?php echo $news_info['news_id'] ?>&popular=<?php echo $news_info['popular']; ?>"><?php echo $news_info['news_title']; ?> </a></h4>
                <em><b class="fa fa-clock-o"></b> <?php echo $news_info['publication_date']; ?>  </em>
                <p><?php echo $news_info['news_short_desc']; ?> </p>
            </div>
            
        </div>
      
        <?php } ?>
    </div>
</div>