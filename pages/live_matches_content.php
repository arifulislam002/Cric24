<?php 
    $query_result=$obj_app->show_live_matches();
?>

<div class="featured-news">
    <h5><span>Live Matches</span></h5>
    <h5>Date: <?php echo date('l\, F jS\, Y ') ?></h5>
    <table width="100%" height="auto" border="10px">
        <tr>
            <th>Tournament</th>
            <th>Match</th>
            <th>Starts At</th>
        </tr>
        <?php while ($fixture_info=mysqli_fetch_assoc($query_result)){ ?>
        <tr>
            <td><?php echo $fixture_info['tournament_name'];?></td>
            <td><?php echo $fixture_info['country_1'].'&nbsp;&nbsp;VS&nbsp;&nbsp'.$fixture_info['country_2'].'&nbsp;&nbsp;'.$fixture_info['match_no'].'&nbsp;&nbsp;'.$fixture_info['match_type'];?></td>
            <td><?php echo $fixture_info['match_time_gmt'].'(GMT)'.'</br>'.$fixture_info['match_time_local'].'(Local)';?></td>
        </tr>
         <?php } ?>
    </table>
 </div>