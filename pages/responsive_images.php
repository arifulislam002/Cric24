<?php
$query_result = $obj_app->select_all_news_info();
?>
<div id="bl-featured">
    <?php while($news_info=mysqli_fetch_assoc($query_result)) {?>
    <div class="bl-featured-big">
        <img src="pages/<?php echo $news_info['news_img']; ?>"  class="img-responsive" alt=""/>
        <div class="bl-info">
            <span><?php echo $news_info['news_type']; ?> </span>
            <h3><a href=""><?php echo $news_info['news_title']; ?></a></h3>
        </div>
    </div>
    <?php } ?>
</div>